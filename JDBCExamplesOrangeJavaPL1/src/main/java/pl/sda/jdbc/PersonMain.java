package pl.sda.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMain {

    public static void main(String[] args) {

        //insertIntoPerson();

//        selectFromPerson();
//        selectFromPersonPreparedStatement();

//        updateLastNameForPesel();
//        deleteForPesel();
        JDBCUtil.executeStoredProcedureSelectAllFromPerson();
    }

    private static void insertIntoPerson() {
        String insertIntoPerson = "INSERT INTO person VALUES (3, 'Michał', 'Nowak', '89121212341')";
        JDBCUtil.execute(insertIntoPerson);
    }

    private static void selectFromPerson() {
        String firstname = "Michał";
        String selectFromPerson = "SELECT * FROM person p WHERE p.firstName = '" + firstname + "'";
        JDBCUtil.executeQuerySelectFromPerson(selectFromPerson);
    }

    private static void selectFromPersonPreparedStatement() {
        String firstName = "Jan";
        String selectFromPerson = "SELECT * FROM person p WHERE p.firstName = ?";
        JDBCUtil.executePreparedStatementSelectFromPerson(selectFromPerson, firstName);
    }

    private static void updateLastNameForPesel() {
        String pesel = "89121212341";
        String newLastName = "Wołodyjowski";
        String updatePerson = "UPDATE person SET lastName = ? WHERE pesel = ?";
        JDBCUtil.executePreparedStatementUpdatePerson(updatePerson, newLastName, pesel);
    }

    private static void deleteForPesel() {
        String pesel = "89121212341";
        String deleteFromPerson = "DELETE FROM person WHERE pesel = ?";
        JDBCUtil.executePreparedStatementDeleteFromPerson(deleteFromPerson, pesel);
    }

}
